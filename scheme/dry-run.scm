;; dry-run.scm - run a script and show the frames it produces.
;; This file is part of Idest
;; Copyright (C) 2011, 2015-2017 Sergey Poznyakoff
;; Idest is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; Idest is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with Idest.  If not, see <http://www.gnu.org/licenses/>.

(let* ((cmd (command-line))
       (progname (car cmd)))
  (set! %load-hook (lambda (filename)
		     (format #t "~A: loading ~a ...\n"
			     progname filename)))
  (cond
   ((< (length cmd) 3)
    (format (current-error-port) "usage: idest -S ~A SCRIPT FILE...

Normally you don't need to run this program manually.  Instead please
use: idest --dry-run [SCRIPTING OPTIONS]~%"
	    progname)
    (exit 1))
   (else
    (let ((file-name (list-ref cmd 1)))
      (set-program-arguments (cons (car cmd) (list-tail cmd 2)))
      ; Note: a bug in Guile 2.0.9 causes %load-hook to be called twice
      (primitive-load-path file-name)
      (cond
       (idest-readonly
	(format (current-error-port)
		"~A: info: ~A does not modify frames~%" progname file-name))
       ((catch #t
	       (lambda ()
		 (procedure? idest-main))
	       (lambda (key . args)
		 #f))
	(set! idest-readonly #t)
	(let ((main-func idest-main))
	  (set! idest-main
		(lambda (file frames)
		  (let ((result (main-func file frames)))
		    (format #t "File ~A~%" file)
		    (cond
		     ((not result)
		      (format #t "No modifications~%"))
		     ((null? result)
		      (format #t "Would delete all frames~%"))
		     (else
		      (for-each (lambda (frame)
				  (display frame)
				  (newline))
				result))))))))
       (else
	(format (current-error-port)
		"~A: idest-main is not defined in ~A~%" progname file-name)
	(exit 1)))))))

