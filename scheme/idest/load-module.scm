;; This file is part of Idest
;; Copyright (C) 2011, 2017 Sergey Poznyakoff
;; Idest is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; Idest is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with Idest.  If not, see <http://www.gnu.org/licenses/>.

(define-module (idest load-module))

(use-modules (guile-user))

(define-public (idest-load-module type)
  (let* ((cmd (command-line))
	 (mod-pathname #f)
	 (mod-name (list-ref cmd 1)))
       
    (set-program-arguments (list-tail cmd 1))
    
    (letrec ((module (resolve-module
		      (list 'idest type (string->symbol mod-name))))
	     (get-proc (lambda (sym)
			 (if (module-defined? module sym)
			     (let ((proc (module-ref module sym)))
			       (cond
				((procedure? proc)
				 proc)
				(else
				 (format (current-error-port)
					 "idest: ~A is defined in ~A, but is not a procedure~%"
					 sym mod-name)
				 (exit 1))))
			     #f))))

      (cond
       ((not (and module (module-public-interface module)))
	(error "no code for module" mod-name))
       ((get-proc 'idest-init) =>
	(lambda (proc)
	  (proc))))
      
      (cond
;;        ((and (eq? type 'format) (not idest-readonly))
;; 	(format (current-error-port) "idest: ~A wants to modify files!~%"
;; 		mod-pathname)
;; 	(exit 1))
       ((get-proc 'idest-main) =>
	(lambda (proc)
	  (set! idest-main proc)))
       (else
	(format (current-error-port)
		"idest: ~A does not define ~A~%" mod-name 'idest-main)
	(exit 1))))))
