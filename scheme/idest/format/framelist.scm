;; This file is part of Idest
;; Copyright (C) 2011, 2017 Sergey Poznyakoff
;;
;; Idest is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; Idest is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with Idest.  If not, see <http://www.gnu.org/licenses/>.

(define-module (idest format framelist))

(use-modules (ice-9 getopt-long))

(define-public description
  "display a list of frames defined in each file")

(define-public idest-main #f)

(define-public (idest-init)
  (let* ((cmd (command-line))
	 (progname (car cmd))
	 (delim #\newline)
	 (addinfo #f)
	 (frame-list #f) ; FIXME: Need a way to access filter_list from idest
	 (grammar `((full (single-char #\F))
		    (qualified (single-char #\Q))
		    (frames (single-char #\f) (value #t))
		    (single-line (single-char #\l))
		    (help (single-char #\h)))))
    
    (catch 'misc-error
	   (lambda ()
	     (for-each
	      (lambda (x)
		(case (car x)
		  ((full)
		   (set! addinfo
			 (lambda (attr-list)
			   (for-each
			    (lambda (attr)
			      (if (not (eq? (car attr) 'text))
				  (format #t " ~A=\"~A\""
					  (car attr) (cdr attr))))
			    attr-list))))
		  ((qualified)
		   (set! addinfo
			 (lambda (attr-list)
			   (for-each
			    (lambda (attr)
			      (if (not (or (eq? (car attr) 'text)
					   (eq? (car attr) 'descr)))
				  (format #t ":~A" (cdr attr))))
			    attr-list))))
		  ((single-line)
		   (set! delim #\,))
		  ((frames)
		   (set! frame-list (string-split (cdr x) #\,)))
		  ((help)
		   (format #t "usage: idest --format=~A [OPTIONS] FILE...\n"
			   progname)
		   (format #t "displays the frame list\n")
		   (format #t "OPTIONS are:\n")
		   (format #t "  -F, --full         display all qualifiers\n")
		   (format #t "  -f, --frames FLIST display only frames from FLIST\n")
		   (format #t "  -Q, --qualified    display frames in qualified form\n")
		   (format #t "  -l, --single-line  fit output on single-line\n")
		   (format #t "  -h, --help         show this help summary\n")
		   (exit 0))
		  (else
		   (set-program-arguments (cons progname (cdr x))))))
	      (getopt-long cmd grammar)))
	   (lambda (key . args)
	     (with-output-to-port
		 (current-error-port)
	       (lambda ()
		 (format #t "~A: " progname)
		 (apply format #t (list-ref args 1) (list-ref args 2))
		 (newline)
		 (exit 1)))))
    
    (set! idest-main
	  (lambda (name frames)
	    (do ((tail frames (cdr tail)))
		((null? tail))
	      (let ((elt (car tail)))
		(cond
		 ((or (not frame-list) (member (car elt) frame-list))
		  (display (car elt))
		  (if addinfo (addinfo (cdr elt)))
		  (if (null? (cdr tail))
		      (newline)
		      (display delim))))))))))
