;; This file is part of Idest
;; Copyright (C) 2011, 2017 Sergey Poznyakoff
;; Idest is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; Idest is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with Idest.  If not, see <http://www.gnu.org/licenses/>.

(define-module (idest batch setlyrics))

(use-modules (ice-9 getopt-long)
	     (ice-9 rdelim))

(define (show-help progname)
  (format #t "usage: idest --batch=~A [OPTIONS] FILE...\n" progname)
  (format #t "sets the USLT (unsynchronised lyric text) frame from a file\n")
  (format #t "OPTIONS are:\n")
  (format #t "  -f, --file FILE    read text from FILE (default: stdin)\n")
  (format #t "  -l, --lang NAME    set language in which the lyrics is writen (default: eng)\n")
  (format #t "  -c, --content TEXT set content description\n")
  (format #t "  -h, --help         show this help summary\n")
  (exit 0))
	  
(define-public description
  "set song lyrics (USLT frame) from a file")

(define-public idest-main #f)

(define-public (idest-init)
  (let* ((cmd (command-line))
	 (progname (car cmd))
	 (file #f)
	 (lang "eng")
	 (condesc "")
	 (grammar `((file (single-char #\f) (value #t))
		    (lang (single-char #\l) (value #t))
		    (content (single-char #\c) (value #t))
		    (help))))
    (catch 'misc-error
	   (lambda ()
	     (for-each
	      (lambda (x)
		(case (car x)
		  ((file)
		   (set! file (cdr x)))
		  ((lang)
		   (set! lang (cdr x)))
		  ((content)
		   (set! condesc (cdr x)))
		  ((help)
		   (show-help progname))
		  (else
		   (set-program-arguments (cons progname (cdr x))))))
	      (getopt-long cmd grammar)))
	   (lambda (key . args)
	     (with-output-to-port
		 (current-error-port)
	       (lambda ()
		 (format #t "~A: " progname)
		 (apply format #t (list-ref args 1) (list-ref args 2))
		 (newline)
		 (exit 1)))))
    
    (cond
     ((< (length (command-line)) 2)
      (format (current-error-port)
	      "usage: idest -S ~A [OPTIONS] FILE...\n\
Try `idest -S ~A --help', for more info.\n"
	      progname progname)
      (exit 1)))
    
    (let ((text
	   (with-input-from-port
	       (if file (open file O_RDONLY) (current-input-port))
	     (lambda ()
	       (do ((line-list '())
		    (line (read-line) (read-line)))
		   ((eof-object? line)
		    (apply string-append (reverse line-list)))
		 (set! line-list (cons line (cons "\n" line-list))))))))
      (letrec ((mainfunc
		(lambda (name frames)
		  (append
		   (filter
		    (lambda (elt)
		      (not (and (string=? (car elt) "USLT")
				(let ((v (assoc-ref (cdr elt) 'lang)))
				  (or (not v) (string=? lang)))
				(let ((v (assoc-ref (cdr elt) 'condesc)))
				  (or (not v) (string=? condesc))))))
		    frames)
		   (list
		    (cons
		     "USLT"
		     (list (cons 'lang lang)
			   (cons 'condesc condesc)
			   (cons 'text text))))))))
	(set! idest-main mainfunc)))))


