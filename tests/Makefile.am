# This file is part of idest
# Copyright (C) 2011, 2017 Sergey Poznyakoff
#
# Idest is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Idest is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with idest.  If not, see <http://www.gnu.org/licenses/>.

EXTRA_DIST = \
 $(TESTSUITE_AT)\
 testsuite\
 package.m4\
 id3v1\
 id3v1-2\
 id3v2\
 idest-32.png\
 idest-68.png

DISTCLEANFILES       = atconfig
MAINTAINERCLEANFILES = Makefile.in $(TESTSUITE)


## ------------ ##
## package.m4.  ##
## ------------ ##

$(srcdir)/package.m4: $(top_srcdir)/configure.ac $(srcdir)/Makefile
	$(AM_V_GEN){                                      \
	  echo '# Signature of the current package.'; \
	  echo 'm4_define([AT_PACKAGE_NAME],      [@PACKAGE_NAME@])'; \
	  echo 'm4_define([AT_PACKAGE_TARNAME],   [@PACKAGE_TARNAME@])'; \
	  echo 'm4_define([AT_PACKAGE_VERSION],   [@PACKAGE_VERSION@])'; \
	  echo 'm4_define([AT_PACKAGE_STRING],    [@PACKAGE_STRING@])'; \
	  echo 'm4_define([AT_PACKAGE_BUGREPORT], [@PACKAGE_BUGREPORT@])'; \
	} >$(srcdir)/package.m4

#

## ------------ ##
## Test suite.  ##
## ------------ ##

TESTSUITE_AT = \
 copy-v1-00.at\
 copy-v2-00.at\
 copy-v12-00.at\
 del-all-v1.at\
 del-all-v2.at\
 del-all-v12.at\
 del-frame-v1.at\
 del-frame-v2.at\
 delcomm.at\
 fmt-shortlist.at\
 framelist00.at\
 framelist01.at\
 framelist02.at\
 list1.at\
 list2.at\
 lyrics00.at\
 info-v1-00.at\
 info-v2-00.at\
 info-v12-00.at\
 pic00.at\
 pic01.at\
 set-v1-00.at\
 set-v1-01.at\
 set-v2-00.at\
 set-v2-01.at\
 setlyrics.at\
 setpic.at\
 settitle.at\
 script00.at\
 script01.at\
 script02.at\
 script03.at\
 script04.at\
 script05.at\
 script06.at\
 script07.at\
 script08.at\
 shortlist.at\
 testsuite.at\
 query-v1-00.at\
 query-v1-01.at\
 query-v2-00.at\
 query-v2-01.at\
 query-v2-02.at\
 query-v2-03.at\
 query-v2-04.at\
 version.at

TESTSUITE = $(srcdir)/testsuite
M4=m4

AUTOTEST = $(AUTOM4TE) --language=autotest
$(TESTSUITE): package.m4 $(TESTSUITE_AT)
	$(AUTOTEST) -I $(srcdir) testsuite.at -o $@.tmp
	mv $@.tmp $@

atconfig: $(top_builddir)/config.status
	cd $(top_builddir) && ./config.status tests/$@

clean-local:
	test ! -f $(TESTSUITE) || $(SHELL) $(TESTSUITE) --clean

check-local: atconfig atlocal $(TESTSUITE)
	$(SHELL) $(TESTSUITE)

# Run the test suite on the *installed* tree.
#installcheck-local:
#	$(SHELL) $(TESTSUITE) AUTOTEST_PATH=$(exec_prefix)/bin

## ------------ ##
## genfile      ##
## ------------ ##

check_PROGRAMS = genfile

genfile_SOURCES = genfile.c


