/* This file is part of Idest.
   Copyright (C) 2009-2011, 2017 Sergey Poznyakoff
 
   Idest is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Idest is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Idest.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#ifdef HAVE_GETOPT_H
# include <getopt.h>
#endif

static void
copyfile(FILE *file, const char *name)
{
	int c;
	FILE *in = fopen(name, "r");
	if (!in) { 
		perror(name);
		exit(1);
	}
	while ((c = fgetc(in)) != EOF)
		fputc(c, file);
	fclose(in);
}

int
main(int argc, char **argv)
{
	char *suf;
	unsigned int length, i, c;
	char *filename = NULL, *suffix = NULL, *prefix = NULL;
	FILE *fp;
	
	while ((c = getopt(argc, argv, "hf:p:s:")) != EOF) {
		switch (c) {
		case 'f':
			filename = optarg;
			break;

		case 'p':
			prefix = optarg;
			break;

		case 's':
			suffix = optarg;
			break;

		case 'h':
			fprintf(stdout,
				"usage: %s [-f FILE] [-p PREFIX]"
				" [-s SUFFIX] SIZE\n", argv[0]);
			exit(0);

		default:
			exit(1);
		}
	}

	if (optind + 1 != argc) {
		fprintf(stderr, "%s: invalid usage\n", argv[0]);
		exit(1);
	}

	length = strtoul(argv[optind], &suf, 10);
	if (suf[0] && suf[1]) {
		fprintf(stderr, "%s: unknown size suffix: %s\n", argv[0], suf);
		exit(1);
	}
	
	switch (*suf) {
	case 0:
		break;
	case 'g':
	case 'G':
		length *= 1024;
	case 'm':
	case 'M':
		length *= 1024;
	case 'k':
	case 'K':
		length *= 1024;
		break;
	default:
		fprintf(stderr, "%s: unknown size suffix: %s\n", argv[0], suf);
		exit(1);
	}

	if (filename) {
		fp = fopen(filename, "w");
		if (!fp) {
			perror(filename);
			exit(1);
		}
	} else
		fp = stdout;

	if (prefix)
		copyfile(fp, prefix);
	
	for (i = 0; i < length; i++)
		fputc (i & 255, fp);

	if (suffix)
		copyfile(fp, suffix);
	
	if (ferror(fp)) {
		fprintf(stderr, "%s: write error", argv[0]);
		exit(1);
	}
	fclose(fp);
	exit(0);
}
	
