# This file is part of idest  -*- autotest -*-
# Copyright (C) 2011, 2015-2017 Sergey Poznyakoff
#
# Idest is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Idest is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with idest.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([formats: lyrics])
AT_KEYWORDS([script format lyrics lyrics00])

AT_CHECK([
WITH_GUILE([
genfile -f file -p $abs_srcdir/id3v2 10k
unset PAGER
idest -N -p $abs_top_srcdir/scheme --format lyrics file])
],
[0],
[File

IdEst is an ID3 Edit and Scripting Tool, a command line utility for
manipulating ID3 tags.  ID3 tag is a metadata container which supplies
related information for a MP3 audio file.  It allows information such
as the title, artist, album, track number, etc. to be stored in the file
itself.

The idest utility allows to create new tags, and to view,
modify or delete the existing ones.  When compiled with Guile,
IdEst allows you to write programs of arbitrary complexity for
manipulating ID3 tags and to apply them to any number of files.
])

AT_CLEANUP
