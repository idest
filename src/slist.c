/* This file is part of Idest.
   Copyright (C) 2009-2011, 2017 Sergey Poznyakoff
 
   Idest is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Idest is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Idest.  If not, see <http://www.gnu.org/licenses/>. */

#include "idest.h"

static bool
sl_eq(const void *elt1, const void *elt2)
{
	return strcmp(elt1, elt2) == 0;
}

static void
sl_dispose(const void *elt)
{
	free((void*)elt);
}

gl_list_t
new_string_list(bool allow_duplicates)
{
	return gl_list_create_empty(&gl_linked_list_implementation,
				    sl_eq,
				    NULL,
				    sl_dispose,
				    allow_duplicates);
}

int
do_string_list(gl_list_t list, string_list_action_fn action, void *data)
{
	int rc = 0;
	gl_list_iterator_t itr = gl_list_iterator(list);
	const void *p;
	while (gl_list_iterator_next(&itr, &p, NULL)) 
		if ((rc = action((const char*)p, data)))
			break;
	gl_list_iterator_free(&itr);
	return rc;
}

void
concat_string_list(gl_list_t list, gl_list_t addlist)
{
	gl_list_iterator_t itr = gl_list_iterator(addlist);
	const void *p;
	while (gl_list_iterator_next(&itr, &p, NULL)) 
		gl_list_add_last(list, xstrdup((const char*)p));
	gl_list_iterator_free(&itr);
}

static int
_sl_printer(const char *str, void *data)
{
	FILE *fp = data;
	fprintf(fp, " %s", str);
	return 0;
}

void
print_string_list(FILE *fp, gl_list_t list)
{
	do_string_list(list, _sl_printer, fp);
}
