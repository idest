/* This file is part of Idest.
   Copyright (C) 2009-2011, 2015-2017 Sergey Poznyakoff
 
   Idest is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Idest is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Idest.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <error.h>
#include <progname.h>
#include <xalloc.h>
#include <gl_linked_list.h>
#include <gl_xlist.h>
#include <backupfile.h>
#include <id3tag.h>
#include <frametype.h>
#include <assert.h>
#include <locale.h>

#define gettext(s) s
#define _(s) s
#define N_(s) s

#define DEFAULT_ED_LIST "title,album,track,comment,artist,year,genre"

#define IDEST_ID3V_1 0x01
#define IDEST_ID3V_2 0x02

enum item_id {
	item_title,
	item_artist,
	item_album,
	item_year,
	item_comment,
	item_track,
	item_genre
};

#define ED_ITEM_CONCAT 0x01

struct ed_item {
	char *name;                /* Printable name */
	char id[5];                /* Item ID */
	int qc;                    /* Number of name qualifiers */
	char **qv;                 /* Name qualifiers */
	char *value;               /* Value */
	struct ed_item const *ref; /* Reference item */
};

#define MODE_QUERY  0
#define MODE_MOD    1
#define MODE_DELETE 2
#define MODE_INFO   3
#define MODE_LIST   4

extern int mode;
extern char const *charset;
extern char const *broken_8bit_charset;
extern enum id3_field_textencoding textual_frame_encoding;
extern int describe_option;
extern int verbose_option;
extern int all_frames;
extern char *source_file;
extern struct id3_tag *source_tag;
extern int source_vopt;
extern enum backup_type backup_type;
extern char *backup_dir;
extern unsigned convert_version;
extern unsigned version_option;
extern unsigned default_version_option;
extern int guile_debug;
extern char **guile_argv;
extern int no_init_files_option;
extern int fixup_option;

/* idop.c */
int guess_file_tag_options(struct id3_file *file, int *modified);
int set_frame_value(struct id3_frame *frame, const struct ed_item *item);
void set_tags(const char *name);
void del_tags(const char *name);
void query_tags(const char *name);
void info_id3(const char *name);

char *field_to_string(union id3_field *field, struct id3_frame const *frame);
int field_binary_from_string(union id3_field *field, const char *str);
char *field_binary_to_string(union id3_field *field);

int frame_field_from_rawdata(struct id3_frame *frame, int n,
			     enum id3_field_type type,
			     const char *value);

void input_list_add_assignment(const char *name, const char *value);

void list_supported_frames(void);
	

/* slist.c */
typedef int (*string_list_action_fn) (const char *, void *);

gl_list_t new_string_list(bool allow_duplicates);
int do_string_list(gl_list_t list, string_list_action_fn action, void *data);
void concat_string_list(gl_list_t list, gl_list_t addlist);
void print_string_list(FILE *fp, gl_list_t list);

/* frametab.gperf */
#define IDEST_OK           0
#define IDEST_ERR_NOFIELD  1
#define IDEST_ERR_BADCONV  2
#define IDEST_ERR_SET      3
#define IDEST_ERR_NOTSUPP  4
#define IDEST_ERR_BADTYPE  5
#define IDEST_ERR_BADFIELD 6

extern char *_idest_errstr[];
extern int _idest_nerrs;
const char *idest_strerror(int code);

typedef int (*idest_frame_cmp_t) (struct id3_frame const *frame,
				  const struct ed_item const *item);
typedef int (*idest_frame_encoder_t) (struct id3_frame *frame,
				      const struct ed_item const *item);
typedef int (*idest_frame_decoder_t) (struct ed_item *pitem,
				      struct id3_frame const *frame);


struct idest_frametab {
	const char id[5];
	int qc;
	char **qv;
	idest_frame_cmp_t    cmp;
	idest_frame_encoder_t encode;
	idest_frame_decoder_t decode;
};

const struct idest_frametab *idest_frame_lookup(const char *str);
struct id3_frame *find_matching_frame(struct id3_tag *tag,
				      const struct ed_item *item,
				      idest_frame_cmp_t cmp);
int frame_field_from_string(struct id3_frame *frame, int n, const char *value);
int frametab_enumerate(int (*fun)(const struct idest_frametab *, void *),
		       void *data, int sorted);


/* main.c */
const char *name_to_frame_id(const char *name);
int frame_to_item_id(const char *arg);
void qv_free(size_t qc, char **qv);
void parse_filter_items(const char *arg);
void output_list_append(struct ed_item const *item, struct ed_item const *ref);
void output_list_print(void);
void output_list_free(void);


/* editem.c */
void ed_item_zero(struct ed_item *item);
void ed_item_free(struct ed_item *item);
void ed_item_free_content(struct ed_item *item);
int ed_item_qv_cmp(struct ed_item const *a, struct ed_item const *b);
struct ed_item *ed_item_create0(char *name, const char *id);
struct ed_item *ed_item_create(const char *name, const char *id);
struct ed_item *ed_item_dup(struct ed_item const *orig);
struct ed_item *ed_item_from_frame_spec(const char *arg, size_t len);
gl_list_t ed_list_create(void);
void ed_item_print(struct ed_item const *itm);
struct ed_item const *ed_list_locate(gl_list_t list,
				     struct id3_frame *frame,
				     idest_frame_cmp_t cmp);

const char *str_split_col(const char *str, size_t *plen, size_t *pn);


/* backup.c */
int backup_file(const char *file);


/* guile.c */
void guile_init(int *pargc, char ***pargv);
int guile_transform(const char *file, struct id3_tag *tag);
int guile_list(const char *file, struct id3_tag *tag);
void guile_add_load_path(const char *arg, int li);

/* utf8conv.c */
enum idest_conv_mode {
	idest_conv_decode, /* convert from UTF-8 to the required charset */
	idest_conv_encode, /* convert from required charset to UTF-8 */
	idest_conv_recode, /* convert from broken 8bit charset to UTF-8 */
};

int utf8_convert(int mode, char const *input, char **output);


