/* This file is part of Idest.
   Copyright (C) 2009-2011, 2017 Sergey Poznyakoff
 
   Idest is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.
 
   Idest is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with Idest.  If not, see <http://www.gnu.org/licenses/>. */

#include "idest.h"

void
ed_item_zero(struct ed_item *item)
{
	memset(item, 0, sizeof(*item));
}

void
ed_item_free_content(struct ed_item *item)
{
	qv_free(item->qc, item->qv);
	free(item->value);
	free(item->name);
}

void
ed_item_free(struct ed_item *item)
{
	qv_free(item->qc, item->qv);
	free(item->value);
	free(item->name);
	free(item);
}

#define ISWILD(s) ((s)[0] == '*' && (s)[1] == 0)

int
ed_item_qv_cmp(struct ed_item const *a, struct ed_item const *b)
{
	int i, count;

	count = a->qc < b->qc ? a->qc : b->qc;
	for (i = 0; i < count; i++) {
		if (!a->qv[i] || !b->qv[i] ||
		    ISWILD(a->qv[i]) || ISWILD(b->qv[i]))
			continue;
		if (strcmp(a->qv[i], b->qv[i]))
			return 1;
	}
	return 0;
}

struct ed_item *
ed_item_create0(char *name, const char *id)
{
	struct ed_item *itm = xzalloc(sizeof(itm[0]));
	itm->name = name;
	strncpy(itm->id, id, sizeof(itm->id));
	return itm;
}

struct ed_item *
ed_item_create(const char *name, const char *id)
{
	return ed_item_create0(xstrdup(name), id);
}

struct ed_item *
ed_item_dup(struct ed_item const *orig)
{
	int i;
	struct ed_item *copy;
	
	copy = ed_item_create(orig->name, orig->id);
	copy->qc = orig->qc;
	copy->qv = xcalloc(orig->qc, sizeof(orig->qv[0]));

	for (i = 0; i < orig->qc; i++)
		copy->qv[i] = orig->qv[i] ? xstrdup(orig->qv[i]) : NULL;
	copy->value = orig->value ? xstrdup(orig->value) : NULL;
	return copy;
}

const char *
str_split_col(const char *str, size_t *plen, size_t *pn)
{
	size_t n;
	size_t len = *plen;
	
	for (n = 0; len; len--, n++, str++) {
		if (*str == ':') {
			*pn = n;
			*plen = len - 1;
			return str + 1;
		}
	}
	*pn = n;
	*plen = len;
	return NULL;
}

static int
split_item_name(struct ed_item *itm, const char *arg, size_t len)
{
	const char *p;
	size_t n;

	p = str_split_col(arg, &len, &n);
	itm->name = xmalloc(n + 1);
	memcpy(itm->name, arg, n);
	itm->name[n] = 0;

	if (p) {
		size_t qc;
		int i;
		
		/* count qualifier elements */
		for (qc = 1, i = 0; i < len; i++)
			if (p[i] == ':')
				qc++;

		itm->qc = qc;
		itm->qv = xcalloc(qc, sizeof(itm->qv[0]));

		for (i = 0; len && i < qc; i++) {
			char *s;

			arg = p;
			p = str_split_col(arg, &len, &n);
			if (n == 0)
				s = NULL;
			else {
				s = xmalloc(n + 1);
				memcpy(s, arg, n);
				s[n] = 0;
			}
			itm->qv[i] = s;
		}
	} else {
		itm->qc = 0;
		itm->qv = NULL;
	}
	return 0;
}

struct ed_item *
ed_item_from_frame_spec(const char *arg, size_t len)
{
	struct ed_item itm;
	struct id3_frametype const *frametype;
	struct idest_frametab const *ft;
	size_t n;
	const char *id;
	char *idstr;
	
	ed_item_zero(&itm);
	if (split_item_name(&itm, arg, len))
		error(1, 0, "ivalid frame spec: %s", arg);

	n = strcspn(itm.name, "%");
	if (itm.name[n]) {
		char *s = xmalloc(n + 1);
		memcpy(s, itm.name, n);
		s[n] = 0;

		idstr = xstrdup(itm.name + n + 1);
		free(itm.name);
		itm.name = s;
	} else
		idstr = itm.name;
	
	id = name_to_frame_id(idstr);
	if (!id) {
		frametype = id3_frametype_lookup(idstr, strlen(idstr));
		if (!frametype) 
			error(1, 0, "%s: unknown frame name", idstr);
		id = idstr;
	} else {
		frametype = id3_frametype_lookup(id, 4);
		assert(frametype != NULL);
	}

	ft = idest_frame_lookup(id);
	if (!ft)
		error(1, 0, "don't know how to handle frame %s", id);

	memcpy(itm.id, id, sizeof(itm.id));

	if (idstr != itm.name)
		free(idstr);
	
	if (itm.qc > ft->qc)
		error(1, 0,
		      "too many field qualifiers for this frame: %s", arg);
	
	if (describe_option) {
		free(itm.name);
		itm.name = xstrdup(frametype->description);
	}

	return ed_item_dup(&itm);
}

void
ed_item_print(struct ed_item const *itm)
{
	printf("%s", itm->name);
	if (itm->qc) {
		int i;

		for (i = 0; i < itm->qc; i++)
			printf(":%s", itm->qv[i] ? itm->qv[i] : "");
	}
	printf(": %s\n", itm->value ? itm->value : "");
}

static bool
ed_item_eq(const void *elt1, const void *elt2)
{
	const struct ed_item *i1 = elt1;
	const struct ed_item *i2 = elt2;
	return strcmp(i1->id, i2->id) == 0;
}

static void
ed_item_dispose(const void *elt)
{
	struct ed_item *itm = (struct ed_item *) elt;
	ed_item_free(itm);
}

gl_list_t 
ed_list_create()
{
	return gl_list_create_empty(&gl_linked_list_implementation,
				    ed_item_eq,
				    NULL,
				    ed_item_dispose,
				    false);
}

struct ed_item const *
ed_list_locate(gl_list_t list, struct id3_frame *frame, idest_frame_cmp_t cmp)
{
	gl_list_iterator_t itr;
	const void *p;
	const struct ed_item *item = NULL;
	
	itr = gl_list_iterator(list);
	while (gl_list_iterator_next(&itr, &p, NULL)) {
		item = p;
		if (memcmp(item->id, frame->id, 4) == 0 &&
		    (!cmp || item->qc == 0 || cmp(frame, item) == 0))
			break;
		item = NULL;
	}
	gl_list_iterator_free(&itr);
	return item;
}










