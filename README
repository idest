IdEst README
Copyright (C) 2009-2011 Sergey Poznyakoff

* Introduction

This file contains brief information about configuring, testing
and running IdEst.  It is *not* intended as a replacement
for the documentation, it is provided as a brief reference only.
The complete documentation for IdEst is available in info format.
To read it without installing the package run `info -f doc/idest.info'.
After installation, the documentation can be accessed running
`info idest'. 

An online copy of the documentation in various formats is available
at http://idest.man.gnu.org.ua.

* Overview

IdEst is an ID3 Edit and Scripting Tool, a command line utility for
manipulating ID3 tags.  ID3 tag is a metadata container which supplies
related information for a MP3 audio file.  It allows the information such
as the title, artist, album, track number, etc. to be stored in the file
itself.

The idest utility allows to create new tags, and to view,
modify or delete the existing ones.  When compiled with Guile,
IdEst allows to write programs of arbitrary complexity for manipulating
ID3 tags and to apply them to any number of files. 

* Building

The usual sequence applies:

  ./configure
  ./make
  ./make install

Refer to the INSTALL file for information about generic configure
options.  The options specific for this package are:

**  --with-guile
This option enables Guile support.  It is the default.  Guile is the base
of IdEst scripting facility.  A Guile version 1.8.3 or later is required
for this option to work.  To compile without Guile support, use
--without-guile.

* Bug reporting.		

Send bug reports to <bug-idest@gnu.org.ua>. 


* Copyright information:

Copyright (C) 2009-2011 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.


Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:

