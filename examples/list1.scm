;; list1.scm -- lists all frames.
;; Copyright (C) 2011 Sergey Poznyakoff
;; License GPLv3+: GNU GPL version 3 or later
;;  <http://gnu.org/licenses/gpl.html>
;; This is free software: you are free to change and redistribute it.
;; There is NO WARRANTY, to the extent permitted by law.

(define (idest-main name frames)
  (display name)
  (newline)
  (for-each
   (lambda (frame)
     (display frame)
     (newline))
   frames))
