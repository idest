;; list2.scm -- lists only requested frames.
;; Copyright (C) 2011 Sergey Poznyakoff
;; License GPLv3+: GNU GPL version 3 or later
;;  <http://gnu.org/licenses/gpl.html>
;; This is free software: you are free to change and redistribute it.
;; There is NO WARRANTY, to the extent permitted by law.

(define frame-list '())

(define (idest-main name tags)
  (display name)
  (newline)
  (for-each
   (lambda (tag)
     (if (member (car tag) frame-list)
	 (begin
	   (display tag)
	   (newline))))
   tags))

(let ((cmd (command-line)))
  (cond
   ((< (length cmd) 3)
    (error "usage: list2.scm FRAME-LIST FILE...")
    (exit 1))
   (else
    (set! frame-list (string-split (list-ref cmd 1) #\,))
    (set-program-arguments (cons (car cmd) (list-tail cmd 2))))))
