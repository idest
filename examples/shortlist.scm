;; shortlist.scm - List file name, artist and year, on a single line.
;; Copyright (C) 2011 Sergey Poznyakoff
;; License GPLv3+: GNU GPL version 3 or later
;;  <http://gnu.org/licenses/gpl.html>
;; This is free software: you are free to change and redistribute it.
;; There is NO WARRANTY, to the extent permitted by law.

(define (get-frame code frames)
  (or (assoc-ref
       (or (assoc-ref frames code) '())
       'text)
      "unknown"))

(define (idest-main name frames)
  (format #t "~A: ~A by ~A, ~A~%"
	  name
	  (get-frame "TIT2" frames)   ; Title
	  (get-frame "TPE1" frames)   ; Artist
	  (get-frame "TDRC" frames))) ; Year
