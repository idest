;; title.scm - set title (TIT2) frame based on the file name.
;; Copyright (C) 2011, 2015 Sergey Poznyakoff
;; License GPLv3+: GNU GPL version 3 or later
;;  <http://gnu.org/licenses/gpl.html>
;; This is free software: you are free to change and redistribute it.
;; There is NO WARRANTY, to the extent permitted by law.

(use-modules (ice-9 regex)
	     (srfi srfi-13))

(define (idest-main file frames)
  (cond
   ((string-match "(.*/)?(.*)\\.mp3" file) =>
    (lambda (match)
      (cons
       (cons "TIT2"
	     (list
	      (cons
	       'text
	       (string-map
		(lambda (c)
		  (if (char=? c #\_) #\space c))
		(match:substring match 2)))))
       ;;
       (filter
	(lambda (elt)
	  (not (string=? (car elt) "TIT2")))
	frames))))
   (else
    #f)))

(set! idest-readonly #f)
